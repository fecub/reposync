#!/bin/sh

SOURCE_SYNC=/home/deploy/RepoSync/bitbucket/smartbannerapp
DESTINATION_SYNC=/home/deploy/RepoSync/gitlab/core_sync_testing

# Change to source folder for sync
cd $SOURCE_SYNC

# checkout default branch
git checkout staging

while true
do
    # Change to source folder for sync
    cd $SOURCE_SYNC
    
    # get last commits for checking
    git fetch
    
    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
        # echo "`date +%T` [Up-to-date] Bitbucket-Gitlab Repository "
        echo "[Up-to-date] SmartBannerApp Repository"
    elif [ $LOCAL = $BASE ]; then
        # before change to destination folder for syncing pull latest commits
        git pull

        # change to destination folder for sync
        cd $DESTINATION_SYNC
        git checkout staging
    echo "[Need to pull] SmartBannerApp Repository"
    echo "[ * ] Pulling new base from bitbucket"
        git pull sync  staging
    echo "[ ✔ ] Pulling new base from bitbucket"
    elif [ $REMOTE = $BASE ]; then
        echo "[ * ] Pushing new base to gitlab"
            git push -u origin staging
        echo "[ ✔ ] Pushing new base to gitlab"
    else
        echo "[ FAIL ] Diverged"
    fi
    
    sleep 5
done
